# README #

1/3スケールのSHARP X-1用データレコーダ風小物のstlファイルです。 スイッチ類、I/Oポート等は省略しています。 
組み合わせたファイルとパーツごとに配置したファイルの二種類があります。 元ファイルはAUTODESK 123D DESIGNです。 

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_cz-8rl1/raw/0dfc8c60de5a68cf0959cd233bcb827543d2a743/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_cz-8rl1/raw/0dfc8c60de5a68cf0959cd233bcb827543d2a743/ModelView_open.png)